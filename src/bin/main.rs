// https://doc.rust-lang.org/book/ch20-01-single-threaded.html
use std::thread;
use std::time::Duration;
use std::fs;
use std::io::prelude::*;
use std::net::TcpStream;
use std::net::TcpListener;

use rust_book_project::ThreadPool;

fn main() {
    // Create new TCP listener, bind it to 7878, and unwrap it in to a Result
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    // Define a thread pool with 4 threads
    let pool = ThreadPool::new(4);
    
    // Loop through the stream iterator 
    for stream in listener.incoming() {
        // unwrap the stream
        let stream = stream.unwrap();

        // Using the thread pool 
        pool.execute(|| {
            // call the handler
            handle_connection(stream);
        });
    }
}

fn handle_connection(mut stream: TcpStream) {
    // Create a buffer
    let mut buffer = [0; 512];
    // Unwrap the stream, mut because it might read more data than we asked for and save 
    // that data for the next time, i.e. an internal state change
    stream.read(&mut buffer).unwrap();
    // Define the GET response
    let get = b"GET / HTTP/1.1\r\n";
    let sleep = b"GET /sleep HTTP/1.1\r\n";

    // If buffer starts with a GET response, set variables to return 200 and hello.html
    let (status_line, filename) = if buffer.starts_with(get) {
        ("HTTP/1.1 200 OK\r\n\r\n", "hello.html")
    } else if buffer.starts_with(sleep) {
        // simulate a slow connection
        thread::sleep(Duration::from_secs(5));
        ("HTTP/1.1 200 OK\r\n\r\n", "hello.html")
    } else {
        // else set variables to return 404 and 404.html
        ("HTTP/1.1 404 NOT FOUND\r\n\r\n", "404.html")
    };

    // Handle the files
    let contents = fs::read_to_string(filename).unwrap();
    let response = format!("{}{}", status_line, contents);

    // Write to the stream and flush it
    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}